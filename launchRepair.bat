:: Created by @13Xg.
:: Copyright 2020 All Rights Reserved.

@echo off

:: Make a temp directory to download latest version.
echo Creating Temp directory...
cd %TEMP%
mkdir sysRepair
cd sysRepair
echo Done!


:: Download latest version
echo.
echo.
echo Downloading latest version...
powershell.exe wget -O baseRepair.bat https://gitlab.com/A13Xg/automated-repair/raw/master/baseRepair.bat
echo Done!

:: Run downloaded script
echo.
echo.
echo Running Automated Task...
baseRepair.bat

PAUSE
:: Script created by A13Xg
:: All rights reserved
:: __________Script created by @13Xg__________
:: *******************************************
:: ***All rights reserved. Copyright 2020.****
:: *******************************************



@echo off
::set LOGFILE=osRepair.log
::call :LOG > %LOGFILE%
::exit /b

:: :LOG

:: Clean Start
cls

:: Update Title
title OS Repair Script - A13Xg

:: Signature
echo __________Script created by @13Xg__________
echo *******************************************
echo ***All rights reserved. Copyright 2020.****
echo *******************************************
echo.
echo.

:: Verify user is running as admin, wait for key press.
echo Are you running as Administrator?
color 04
:: RED on Black

:: Implement this later.
:: "runas /noprofile /user:%USERNAME% cmd"

PAUSE

:: Clear console
cls

color 01
:: Blue on Black

echo Repair Script Started - Please Wait.
echo.
echo.

echo Power Settings > High Performance
powercfg.exe /setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c

title OS Repair Script - Job = DISM

echo Invoking DISM Repair. Verify Unit is Running Win8 or later. Internet Connection Required
echo "|| || || || || || || ||"
echo "\/ \/ \/ \/ \/ \/ \/ \/"
echo Beginning Download...
:: Downloads a refrence image of current OS from MS Servers to repair corruption.
DISM /Online /Cleanup-Image /RestoreHealth

echo.
echo *********************
echo **Download complete**
echo *********************
echo.
echo.
echo.

:: Clear console
cls

color 02
:: Green on Black

echo Beginning System File Checker.
title OS Repair Script - Job = SFC

:: System File Checker. Uses onboard image to verify system structure and integrity.
SFC /scannow

color 06
:: Yellow on Black

:: Clear console
cls

echo Starting Defragmentation operation.
title OS Repair Script - Job = Defrag
echo.

:: Defrag HDD and Trim SSD
defrag.exe /H /U /V /B /L /K /O /C

echo Defragmentation Complete.

:: Clear console
cls

:: Run extended CheckDisk operation on next reboot.
echo y | chkdsk /f /v /x

:: Reboot in 10 sec and inform user.
shutdown -r -t 10 -c "Operation Complete. Restarting in 10 Seconds."

cd %TEMP%
rmdir /s /q sysRepair
